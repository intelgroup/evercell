/*! modernizr 3.6.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-touchevents-setclasses !*/
!function(e,n,t){function o(e){var n=u.className,t=Modernizr._config.classPrefix||"";if(p&&(n=n.baseVal),Modernizr._config.enableJSClass){var o=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(o,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),p?u.className.baseVal=n:u.className=n)}function s(e,n){return typeof e===n}function a(){var e,n,t,o,a,i,r;for(var l in c)if(c.hasOwnProperty(l)){if(e=[],n=c[l],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(o=s(n.fn,"function")?n.fn():n.fn,a=0;a<e.length;a++)i=e[a],r=i.split("."),1===r.length?Modernizr[r[0]]=o:(!Modernizr[r[0]]||Modernizr[r[0]]instanceof Boolean||(Modernizr[r[0]]=new Boolean(Modernizr[r[0]])),Modernizr[r[0]][r[1]]=o),f.push((o?"":"no-")+r.join("-"))}}function i(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):p?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function r(){var e=n.body;return e||(e=i(p?"svg":"body"),e.fake=!0),e}function l(e,t,o,s){var a,l,f,c,d="modernizr",p=i("div"),h=r();if(parseInt(o,10))for(;o--;)f=i("div"),f.id=s?s[o]:d+(o+1),p.appendChild(f);return a=i("style"),a.type="text/css",a.id="s"+d,(h.fake?h:p).appendChild(a),h.appendChild(p),a.styleSheet?a.styleSheet.cssText=e:a.appendChild(n.createTextNode(e)),p.id=d,h.fake&&(h.style.background="",h.style.overflow="hidden",c=u.style.overflow,u.style.overflow="hidden",u.appendChild(h)),l=t(p,e),h.fake?(h.parentNode.removeChild(h),u.style.overflow=c,u.offsetHeight):p.parentNode.removeChild(p),!!l}var f=[],c=[],d={_version:"3.6.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){c.push({name:e,fn:n,options:t})},addAsyncTest:function(e){c.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=d,Modernizr=new Modernizr;var u=n.documentElement,p="svg"===u.nodeName.toLowerCase(),h=d._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):["",""];d._prefixes=h;var m=d.testStyles=l;Modernizr.addTest("touchevents",function(){var t;if("ontouchstart"in e||e.DocumentTouch&&n instanceof DocumentTouch)t=!0;else{var o=["@media (",h.join("touch-enabled),("),"heartz",")","{#modernizr{top:9px;position:absolute}}"].join("");m(o,function(e){t=9===e.offsetTop})}return t}),a(),o(f),delete d.addTest,delete d.addAsyncTest;for(var v=0;v<Modernizr._q.length;v++)Modernizr._q[v]();e.Modernizr=Modernizr}(window,document);

var respons = {
	winState: "",
	smallMedium: false,
	mediumLarge: false,
	sw: 0,
	scrollW: 0,
	large: 1200,
	medium: 800,
	sbWidth: function(){
		$("body").append("<div id='sbarWidth' style='overflow-y: scroll; width: 50px; height: 50px;visibility: hidden'></div>");
		var div = $("#sbarWidth")[0];
		var scrollWidth = div.offsetWidth - div.clientWidth;
		document.body.removeChild(div);
		return scrollWidth;
	},
	sbCheck: function(){
		var b;
		if($(window).height() >= $(document).height()){
			b = false;
		}else{
			b = true;
		}
		return b;
	},
	rCheck : function(){
		if(this.sbCheck()){
			this.sw = $(window).width()+this.scrollW;
		}else{
			this.sw = $(window).width();
		}
	    if(this.sw > this.medium){
	    	if(!this.mediumLarge){
		        /* Вызов функций для больших и средних экранов */
				
				$(".gallery-prod").trigger("destroy.owl.carousel");

		    	this.mediumLarge = true;
	    	}
	    	if(this.sw > this.large){
		        if(this.winState != "large"){
		        	/* Вызов функций для больших экранов */
		        	this.winState = "large";
			    	this.smallMedium = false;
		        };
	    	};
	    };
	    if(this.sw <= this.large){
	    	if(!this.smallMedium){
		        /* Вызов функций для средних и маленьких экранов */
		    	this.smallMedium = true;
	    	}
	    	if(this.sw > this.medium && this.sw <= this.large){
		        if(this.winState != "medium"){
		        	/* Вызов функций для планшетных экранов */

		        	this.winState = "medium";
		        }
		    }else{
		        if(this.winState != "small"){
		        	/* Вызов функций для мобильных экранов */
					$(".gallery-prod").owlCarousel({
						items: 1
					});
		        	this.mediumLarge = false;
		        	this.winState = "small";
		        }
		    };
	    }
	},
	init: function(){
		var $this = this;
		$this.scrollW = $this.sbWidth();
		$this.rCheck();
		$(window).resize(function(){
			$this.rCheck();
		});
	}
};
var $checkoutSlider;
var $gallerySlider;
$(function(){
	respons.init();
	if (Modernizr.touchevents) {
	  $("body").addClass("is-touch");
	};
	$("select.styler, input.styler").styler();
	$(".fancybox").fancybox();
	$(".fancy-aside").fancybox({
		touch: false,
		closeExisting: true,
		afterShow: function(){
			$checkoutSlider.trigger("refresh.owl.carousel");
			setTimeout(function(){
				$checkoutSlider.addClass("show");
			}, 100);
		}
	});
	$(".fancy-search").fancybox({
		touch: false,
		closeExisting: true,
		baseClass: "pp_search",
		beforeShow: function(){
			$(".fancy-search").addClass("active");
		},
		afterShow: function(){
		    var availableTags = [
		      "ActionScript",
		      "AppleScript",
		      "Asp",
		      "BASIC",
		      "C",
		      "C++",
		      "Clojure",
		      "COBOL",
		      "ColdFusion",
		      "Erlang",
		      "Fortran",
		      "Groovy",
		      "Haskell",
		      "Java",
		      "JavaScript",
		      "Lisp",
		      "Perl",
		      "PHP",
		      "Python",
		      "Ruby",
		      "Scala",
		      "Scheme"
		    ];
			$(".pp_search .search-autocomplete").autocomplete({
				source: availableTags,
		    	minLength: 2,
			});
			/*
			$(".search-autocomplete").autocomplete({
				source: function(request, response) {
					$.ajax( {
						url: "search.php",
						dataType: "jsonp",
						data: {
							term: request.term
						},
						success: function( data ) {
							response(data);
						}
					});
				},
		    	minLength: 2,
			});
			*/

			$(".header").one("click", ".fancy-search.active", function(){
				$.fancybox.close();
			});

		},
		beforeClose: function(){
			$(".fancy-search").removeClass("active");
		}
	});



	$(".banner-crsl").owlCarousel({
		items: 1,
		onInitialized: function(e){
			$(e.currentTarget).append("<div class='container'><div class='owl-pagin'>01.</div></div>");
		},
	});
	$(".banner-crsl").on('changed.owl.carousel', function(e) {
		$(".owl-pagin").text("0"+(e.page.index+1)+".");
    });


	$(".crsl_items3").owlCarousel({
		loop: true,
		responsive: {
		    0 : {
				items: 1,
				margin: 25,
		    },
		    520 : {
				items: 2,
				margin: 20,
		    },
		    1200 : {
				items: 3,
				margin: 32,
		    }
		}
	});
	$(".crsl_items2").owlCarousel({
		responsive: {
		    0 : {
				items: 1,
				margin: 25,
				loop: true,
		    },
		    1200 : {
				items: 2,
				margin: 32,
		    }
		}
	});

	$(".crsl_items3_type2").owlCarousel({
		responsive: {
		    0 : {
				loop: true,
				items: 1,
				margin: 25,
		    },
		    520 : {
				loop: true,
				items: 2,
				margin: 20,
		    },
		    1200 : {
				items: 3,
				margin: 32,
		    }
		}
	});


	(function(){ //моб. меню
		$(".menu-main__but").on("click", function(){
			if($(this).hasClass("active")){
				$(this).removeClass("active");
				scrollLock.enablePageScroll($(".menu-main__body")[0]);
			}else{
				$(this).addClass("active");
				scrollLock.disablePageScroll($(".menu-main__body")[0]);
			}
		});
		$("body.is-touch .menu-main__list a").on("click", function(e){
			var dropMenu = $(this).siblings(".menu-main__drop");
			if(dropMenu.length){
				e.preventDefault();
				var but = $(this);
				if(but.hasClass("active")){
					but.removeClass("active");
				}else{
					but.addClass("active");
				}
			}
		});
	})();



	//всплывающая карточка

	(function(){
		var $wrap  = $(".pp-card__wrap"),
			$main = $(".pp-card__info"),
			$gallery = $(".pp-card__gallery"),
			$footer  = $(".pp-card__footer");

		$(".minicard").on("click", function(e){
			if($(this).find(".minicard__hidden").length){

				$gallery.empty();
				$main.empty();
				$footer.empty();

				e.preventDefault();
				var $self = $(this),
					tData = {
						gallery: $self.find(".gallery-mini").clone(true),
						$labels: $self.find(".card__label"),
						name: $self.find(".minicard__name"),
						sku: $self.find(".minicard__sku"),
						quant: $self.find(".card__quantity"),
						price: $self.find(".minicard__price"),
						btns: $self.find(".minicard__btns"),
						detail: $self.find(".minicard__more"),
					};
				$gallery.append(tData.gallery);

				var labelsWrap = $("<div class='pp-card__labels'></div>");
				tData.$labels.each(function(){
					labelsWrap.append($(this).clone());
				});
				$main.append(labelsWrap);
				$main.append(tData.name.clone());
				$main.append(tData.sku.clone());
				$main.append(tData.quant.clone());
				$main.append(tData.price.clone());
				$main.append(tData.btns.clone());

				$footer.append(tData.detail.clone());

				$.fancybox.open({
					src: "#pp-card",
					type : "inline",
					opts : {
						touch: false,
						beforeShow: function(){
							$(".popup .card__quantity input").styler();
						},
						afterShow: function(){



								$(".popup .gallery-mini").sliderPro({
									thumbnailsPosition: 'left',
									width: "100%",
									height: "100%",
									thumbnailWidth: 100,
									thumbnailHeight: 100,
									buttons: false,
									autoplay: false,
									breakpoints: {
										1200: {
											thumbnailsPosition: 'bottom',
										},
										800: {
											thumbnailWidth: 80,
											thumbnailHeight: 80,
											thumbnailsPosition: 'bottom',
										},
									}
								});
						}
					}
				});
			}

		});
	})();

	$(".card__tabs").tabs({
		active: 0
	});


	$checkoutSlider = $(".checkout__pages");
	$checkoutSlider.owlCarousel({
		nav: true,
		items: 1,
		nav: false,
		dots: false,
		touch: false,
		mouseDrag: false,
		touchDrag: false,
		onChanged: function(e){
			var $crsl = $(e.target),
				$titles = $crsl.find(".checkout__page-titles").find("span"),
				current;

			$titles.removeClass("last");
			if(e.item.index || e.item.index === 0){
				if($titles.eq(e.item.index).hasClass("active")){
					$titles.eq(e.item.index+1).removeClass("active");
				}else{
					$titles.eq(e.item.index).addClass("active");
				};
			};

			$titles.filter(".active").last().addClass("last");


		},
		onInitialized: function(e){
			var $crsl = $(e.target),
				$items = $crsl.find(".checkout__page"),
				$titles = $("<div />", {
					class: "checkout__page-titles",
				});

			$items.each(function(i){
				if(i==0){
					$titles.append("<span class='active'>"+ $(this).data("name") +"</span>");
				}else{
					$titles.append("<span>"+ $(this).data("name") +"</span>");
				}
			});
			$crsl.prepend($titles);
		}

	})


	
	$(".checkout__next").on("click", function(e){
		e.preventDefault();
		if($(this).hasClass("disabled")){
			return;
		}else{
			$checkoutSlider.trigger('next.owl.carousel');
		};
	});
	$(".checkout__prev, .checkout__back").on("click", function(e){
		e.preventDefault();
		if($(this).hasClass("disabled")){
			return;
		}else{
			$checkoutSlider.trigger('prev.owl.carousel');
		};
	});
	

	$(".thanks__btns").on("click", function(){
		$.fancybox.close();
	});

	$(".prod-sort__but-inner").on("click", function(e){
		var $self = $(this),
			$dropList = $self.siblings(".prod-sort__drop");
		if($dropList.length){
			e.preventDefault();
			if($self.hasClass("active")){
				$self.removeClass("active");
			}else{
				$(".prod-sort__but-inner").removeClass("active");
				$self.addClass("active");
			}
		}
	});
	$(window).on("click", function(e){
		if(!($(e.target).closest(".prod-sort__item").length)){
			$(".prod-sort__but-inner").removeClass("active");
		};
	});


	(function(){  //Фильтры/соритровка
		var but1Class = ".prod-sort_filter .prod-sort__but";
			but2Class = ".prod-sort__but-inner";
		function updateVal(){
			var cntAll = 0;
			$("#filter .prod-sort__item").each(function(){
				var but = $(this).find(but2Class),
					checked = $(this).find("input[type='checkbox']:checked");
				if(checked.length){
					cntAll += checked.length;
					but.addClass("selected").attr("data-checked", checked.length);
				}else{
					but.removeClass("selected").attr("data-checked", 0);
				}
				
			});
			if(cntAll){
				$(but1Class).addClass("selected").attr("data-checked", cntAll);
			}else{
				$(but1Class).removeClass("selected").attr("data-checked", 0);
			};
		}
		updateVal();

		$("#filter form").on("reset", function(){
			setTimeout(updateVal, 1);
		});
		$("#filter input[type='checkbox']").on("change", function(){
			updateVal();
		});
	})();
	$(".prod-sort").children(".prod-sort__but").on("click", function(){

		var $self = $(this),
			$dropFilters = $(this).siblings(".prod-sort__mobile");

		if($self.hasClass("active")){
			$self.removeClass("active");
		}else{
			$self.addClass("active");
		}



	});

	$(".prod-sort_filter .prod-sort__but").on("click", function(){

		$.fancybox.open({
			src: "#filter",
			type : "inline",
			opts : {
				baseClass: "pp__filters",
				touch: false,
			}
		});
	});


	var $aboutSlider = $(".about-slider__body").owlCarousel({
		items: 1,
		dots: false,
		onInitialized: function(e){
			$(e.target).find(".about-slider__pagin").text(e.item.count);
		}
	});

	$(".about-slider__next").on("click", function(){
		$aboutSlider.trigger('next.owl.carousel');
	});

	$(".about-slider__prev").on("click", function(){
		$aboutSlider.trigger('prev.owl.carousel');
	});



	$(".shops__tabs").tabs({
		active: 0
	});

	$(".faq__item").on("click", function(){
		var $self = $(this);
		if($self.hasClass("active")){
			$self.removeClass("active");
			$(this).find(".faq__answer").slideUp(200);
		}else{
			$self.addClass("active");
			$(this).find(".faq__answer").slideDown(200);
		}
	});


	$("body").on("click", ".minicard__to-basket", function(e){
		var $self = $(this);
		e.preventDefault();
		$self.addClass("load").removeClass("more");
		setTimeout(function(){
			$self.removeClass("load");
			$self.addClass("active");
			setTimeout(function(){
				$self.removeClass("active");
				$self.addClass("more");
			}, 1500);
		}, 1500);
	});
	

	$(".mask-date").mask("99.99.9999",{autoclear: false});
	$(".mask-tel").mask("+7 (999) 999-99-99");
	$(".phone-number").mask("+7 (999) 999-99-99");
	

	$(".orders__main").on("click", function(){
		var $parent = $(this).parent(),
			$detail = $(this).siblings(".orders__detail");
		if($parent.hasClass("active")){
			$parent.removeClass("active");
			$detail.slideUp(200);
		}else{
			$parent.addClass("active");
			$detail.slideDown(200);
		}
	});

	var $historyDetail = $(".orders__detail");
	function historyMob(){
		if(respons.winState == "small"){
			if(respons.sw <= 520){
				$historyDetail.each(function(){
					if(!($(this).hasClass("owl-carousel"))){
						$(this).addClass("owl-carousel").owlCarousel({
							items: 2,
							margin: 16
						});
					};
				});
			}else{
				$historyDetail.each(function(){
					if($(this).hasClass("owl-carousel")){
						$(this).removeClass("owl-carousel");
						$historyDetail.trigger("destroy.owl.carousel");
					}
				});
				
			}
		};
	};
	historyMob();
	$(window).on("resize", function(){
		historyMob();
	});

	$(".category__more").on("click", function(e){
		e.preventDefault();
		if(respons.winState == "small"){
			$(".category__desc-mob").slideDown(300);
			$(this).addClass("hide");
		}else{
			$(".pp-category-body").html($(".category__desc-mob").html());
			$.fancybox.open({
				src: "#pp-category-desc",
				type: "inline",
				opts: {

				}
			});
		}
	});

	$("body").on("click", ".minicard__to-basket, .minicard__to-favor", function(e){
		var cardType = $(this).parents(".card").length?"normal":"mini",
			labelType = $(e.target).hasClass("minicard__to-basket")?"basket":"favor",
			parent, name, quant, src;

		if(cardType == "normal"){
			parent = $(this).parents(".card");
			name = parent.find(".card__name").text();
			src = parent.find(".gallery-prod__item").eq(0).find("img").attr("src");
		}else{
			parent = $(this).parents(".pp-card");
			name = parent.find(".minicard__name").text();
			src = parent.find(".sp-slide").eq(0).find("img").attr("src");
		}
		if(labelType == "basket"){
			quant = parent.find(".card__quantity input").val();
		};

		if(labelType == "basket"){
			name = "Добавлено в корзину";
		}else{
			name = "Добавлено в избранное";
		}
		if(labelType == "basket"){
			showPpLabel({
				name: name,
				quant: quant,
				img: src,
				link: {
					type: "pp",
					href: "#pp-basket"
				}
			});
		}else{
			showPpLabel({
				name: name,
				quant: quant,
				img: src,
				link: "account-favor.html"
			});
		}
	});
	$(".flinks__but").on("click", function(){
		$(this).toggleClass("active");	
	});
});
showPpLabel.timer = 0;
function showPpLabel(param){

	var ppLabel = $(".pp-label"),
		labelContent = $(".pp-label__content"),
		labelPict = $(".pp-label__pict");

	if(ppLabel.hasClass("blocked")){
		return;
	}
	destroy();

	var name = param.name,
		quantity = param.quant,
		img = param.img,
		link = param.link, //Строка с url или объект для открытия поп-апа
		customClass = param.customClass;

	if(name){
		var n = $("<div />", {
			class: "pp-label__name",
			text: name
		});
		labelContent.append(n);
	};
	if(quantity){
		var q = $("<div />", {
			class: "pp-label__quantity",
			text: quantity + " шт."
		});
		labelContent.append(q);
	};
	if(img){
		var i = $("<img />", {
			src: img,
		});
		labelPict.append(i);
	};

	if(link){
		var a = $("<a />");
		if(typeof link == "string"){
			a.attr("href", link);
		}else{
			a.attr("href", link.href);
			a.fancybox({
				touch: false,
				closeExisting: true,
				afterShow: function(){
					$checkoutSlider.trigger("refresh.owl.carousel");
					setTimeout(function(){
						$checkoutSlider.addClass("show");
					}, 100);
				}
			});
		};
		ppLabel.append(a);
	};

	ppLabel.addClass("active");
	ppLabel.one("click.pp-label", function(){
		fEmpty();
	});

	function destroy(){
		clearTimeout(showPpLabel.timer);
		ppLabel.off(".pp-label").removeClass("active blocked");
		labelContent.empty();
		labelPict.empty();
		ppLabel.children("a").remove();
	};
	function fEmpty(){
		ppLabel.removeClass("active");
	};

	showPpLabel.timer = setTimeout(function(){
		fEmpty();
		ppLabel.addClass("blocked");
		setTimeout(destroy, 500);
	}, 3500);

};